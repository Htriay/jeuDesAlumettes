import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JeuAllumettesComponent } from './jeu-allumettes.component';

describe('JeuAllumettesComponent', () => {
  let component: JeuAllumettesComponent;
  let fixture: ComponentFixture<JeuAllumettesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JeuAllumettesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JeuAllumettesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
