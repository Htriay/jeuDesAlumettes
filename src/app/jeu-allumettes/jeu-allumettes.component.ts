import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Joueur } from "../../models/joueur";

@Component({
  selector: 'app-jeu-allumettes',
  templateUrl: './jeu-allumettes.component.html',
  styleUrls: ['./jeu-allumettes.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class JeuAllumettesComponent implements OnInit {
  // Nombre allumettes restantes à prendre dans la partie
  public nombreRestantAllumettes: number;
  // Tableau servant à l'affichage des allumettes lié à nombreRestantAllumettes
  public allumettes: Array<string>;
  // Log qui retranscrit tout ce qui se passe dans la partie
  public logHTML: string;
  // Nombre maximal d'allumettes prenables par tour
  private nombreAllumettesPrenable: number;
  // Nombre d'allumettes au début de la partie
  private nombreAllumettesDebut: number;
  private nombreAllumettesDebutMin: number;
  private nombreAllumettesDebutMax: number;

  public utilisateur: Joueur;
  public ia: Joueur;


  constructor() { 
    this.utilisateur = new Joueur("Utilisateur", "j1");
    this.ia = new Joueur("IA", "j2");
    this.nombreAllumettesDebutMin = 20;
    this.nombreAllumettesDebutMax = 29;
    this.nombreAllumettesDebut = Math.floor(Math.random() * (this.nombreAllumettesDebutMax - this.nombreAllumettesDebutMin) + this.nombreAllumettesDebutMin);
    this.nombreAllumettesPrenable = 3;
    this.nombreRestantAllumettes = this.nombreAllumettesDebut;
    this.logHTML = '';
  }

  ngOnInit(): void {
    this.initJeu();
  }
  
  // Ininitalisation d'une partie
  initJeu(){
    // On initialise le nombre restants d'allumettes
    this.nombreRestantAllumettes = this.nombreAllumettesDebut;
    // On initialise le tableau à utiliser pour l'affichage des (nombreRestantAllumettes) allumettes
    this.allumettes = Array(this.nombreRestantAllumettes).fill('');
    // Calcule du nombre de parties jouées
    var nbParties = this.utilisateur.partiesGagnees+this.ia.partiesGagnees+1;
    this.log("<h1>Partie N°"+ nbParties +"</h1>", "defaut");
    this.determinerPremierjoueur();
  }
  
  // Appele de cette méthode à chaque début de partie pour déterminer le premier joueur
  determinerPremierjoueur(){
    var premierTour = this.nombreRestantAllumettes === this.nombreAllumettesDebut;
    // 50% de chance pour les 2 joueurs
    var randomBool = Math.random() >= 0.5;
    if(premierTour && !randomBool){
      // l'IA débute
      this.iaJoue();
    }
  }
    
  // L'humain joue son tour
  humainJoue(nombreAllumettesJoueJoueur: number){
    this.utilisateur.nombreAllumettesPris = nombreAllumettesJoueJoueur;
    var partieTerminee = this.jouerUnTour(this.utilisateur);
    // Si la partie se termine, on arrête tout
    if(!partieTerminee){
      this.iaJoue();
    }
  }

  // L'IA joue son tour
  iaJoue(){
    this.ia.nombreAllumettesPris = this.nombreRestantAllumettes % (this.nombreAllumettesPrenable + 1) !== 0 ? this.nombreRestantAllumettes % (this.nombreAllumettesPrenable + 1) 
                                 : Math.floor(Math.random() * (this.nombreAllumettesPrenable) + 1);
    this.jouerUnTour(this.ia);
  }

  // On joue un tour du jeu des allumettes
  jouerUnTour(joueur: Joueur){
    this.log("<h4>Tour de "+ joueur.nom +" ...</h4>", joueur.couleurLog);
    this.nombreRestantAllumettes -= joueur.nombreAllumettesPris;
    this.log(joueur.nom +" a retiré "+ joueur.nombreAllumettesPris +" allumettes", joueur.couleurLog);
    // Si le joueur récupère la dernière allumette on désigne le gagnant et on incrémente son compteur de victoire
    if (this.nombreRestantAllumettes <= 0) {
      this.log("<h3>"+ joueur.nom +" a gagné la partie</h3>", joueur.couleurLog);
      joueur.partiesGagnees++;
      this.initJeu();
      return true;
    }
    this.log("Il reste " + this.nombreRestantAllumettes + " allumettes à jouer", "defaut");
    // On rafraichit l'affichage des allumettes
    this.allumettes = Array(this.nombreRestantAllumettes).fill('');
    return false;
  }

  log(message: string, className: string){
    this.logHTML = '<p class="'+ className +'">'+ message +'</p>' + this.logHTML; 
  }

}
