export class Joueur {
    private _nom: string;
    private _nombreAllumettesPris: number;
    private _couleurLog: string;
    // Compteur de parties gagnés pour ce joueur
    public _partiesGagnees: number;

    constructor(nom: string, couleurLog: string){
      this._nom = nom;
      this._couleurLog = couleurLog;
      this._partiesGagnees = 0;
    }

    public get nom(): string {
        return this._nom;
      }

    public set nom(value: string) {
        this._nom = value;
    }

    public get nombreAllumettesPris(): number {
        return this._nombreAllumettesPris;
      }
      
    public set nombreAllumettesPris(value: number) {
        this._nombreAllumettesPris = value;
    }

    public get couleurLog(): string {
        return this._couleurLog;
      }

    public set couleurLog(value: string) {
        this._couleurLog = value;
    }

    public get partiesGagnees(): number {
        return this._partiesGagnees;
      }
      
    public set partiesGagnees(value: number) {
        this._partiesGagnees = value;
    }
    
}
